Adaptation des applications "L'ordre alphabétique" de la compilation d'applications clicmenu.

Classer des mots dans l'ordre alphabétique. Différents niveaux sont proposés.

L'application peut être testée en ligne [ici](https://primtux.fr/applications/ordre-alphabetique/index.html)
